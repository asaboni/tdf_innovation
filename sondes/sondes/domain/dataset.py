from typing import Dict


class Dataset():
    name: str
    technicalName: str
    type: str
    id: str
    versionId: str
    path: str
    attributes: Dict
